/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 17:03:50 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:33:02 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	del_path(t_path *del)
{
	if (del)
	{
		del_path(del->next);
		free(del);
	}
}

void	delete_last(t_path *res_list)
{
	if (res_list && res_list->next)
	{
		while (res_list->next->next)
			res_list = res_list->next;
		free(res_list->next);
		res_list->next = NULL;
	}
	else if (res_list)
		free(res_list);
}

void	clean_up_path(t_path **all_path)
{
	int	i;

	if (all_path)
	{
		i = 0;
		while (all_path[i])
			del_path(all_path[i++]);
		free(all_path);
	}
}

t_path	*choose_path(t_path **ant_path, t_path **all_path)
{
	int	min;
	int	i;

	i = 0;
	min = i;
	while (all_path[i])
	{
		if (moves(all_path[i], ant_path) < moves(all_path[min], ant_path))
			min = i;
		i++;
	}
	return (all_path[min]);
}

int		moves(t_path *curr, t_path **ant_path)
{
	int	res;
	int	i;

	res = count_path(curr);
	i = 1;
	while (ant_path[i])
	{
		if (!ft_strcmp(curr->next->room->name, ant_path[i]->next->room->name))
			res++;
		i++;
	}
	return (res);
}
