/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_unique.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/25 17:01:53 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:29:01 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	leave_unique_only(t_path **all_path)
{
	int	i;
	int	j;

	sort_path(all_path);
	i = 0;
	while (all_path[i])
	{
		j = i;
		while (all_path[++j])
			if (check_unique(&all_path[i], &all_path[j]))
				j--;
		i++;
	}
}

int		check_unique(t_path **path1, t_path **path2)
{
	t_path *temp1;
	t_path *temp2;

	temp1 = *path1;
	temp2 = *path2;
	while (temp1 && temp2)
	{
		if (!ft_strcmp(temp1->room->name, temp2->room->name))
			if (temp1->room->flag != 1 && temp1->room->flag != 2)
			{
				del_path(*path2);
				*path2 = NULL;
				shift_up_elems(path2);
				return (1);
			}
		temp1 = temp1->next;
		temp2 = temp2->next;
	}
	return (0);
}

void	shift_up_elems(t_path **path2)
{
	t_path	**current;
	t_path	**after;

	current = path2;
	after = path2 + 1;
	while (*after || *current)
	{
		*current = *after;
		current = after;
		after = current + 1;
	}
}

void	sort_path(t_path **all_path)
{
	int		i;
	int		j;
	t_path	*temp;

	i = 0;
	while (all_path[i])
	{
		j = i;
		while (all_path[++j])
			if (count_path(all_path[j]) < count_path(all_path[i]))
			{
				temp = all_path[j];
				all_path[j] = all_path[i];
				all_path[i] = temp;
			}
		i++;
	}
}
