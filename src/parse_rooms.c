/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_rooms.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 17:49:50 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/24 13:17:32 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		add_room(t_room **start, char **line, int ants)
{
	t_room	*new;
	t_room	*temp;

	if (!(new = create_room(*line)))
		return (0);
	temp = *start;
	if (temp)
	{
		while (temp->next)
			temp = temp->next;
		temp->next = new;
		if (!ft_strcmp(temp->name, "##start"))
		{
			new->flag = 1;
			new->ant_in = ants;
		}
		if (!ft_strcmp(temp->name, "##end"))
			new->flag = 2;
	}
	else
		*start = new;
	free(*line);
	*line = NULL;
	return (1);
}

int		check_room(char *line)
{
	if (!line || *line == 'L' || *line == '\0')
		return (0);
	if (*line == '#')
		return (1);
	while (*line == ' ')
		line++;
	while (*line && *line != ' ')
		line++;
	if (*line == ' ' && ft_isdigit(*(line + 1)))
		while (ft_isdigit(*++line))
			;
	if (*line == ' ' && ft_isdigit(*(line + 1)))
		while (ft_isdigit(*++line))
			;
	else
		return (0);
	if (!*line)
		return (1);
	return (0);
}

t_room	*create_room(char *line)
{
	t_room	*new;
	char	*tmp;
	int		flag;

	flag = (*line == '#') ? 3 : 0;
	tmp = line;
	if (!(new = (t_room *)malloc(sizeof(t_room))))
		return (NULL);
	new->flag = flag;
	while (*tmp == ' ')
		tmp++;
	while (*tmp && *tmp != ' ')
		tmp++;
	new->name = (flag == 3) ? ft_strdup(line) : ft_strsub(line, 0, tmp - line);
	new->x = (flag == 3) ? 0 : ft_atoi(tmp);
	new->y = (flag == 3) ? 0 : ft_atoi(ft_strrchr(tmp, ' '));
	new->ant_in = 0;
	new->next = NULL;
	new->links = NULL;
	return (new);
}

int		check_logic_room(t_room *list)
{
	t_room	*begin_list;
	int		begin;
	int		end;

	begin_list = list;
	begin = 0;
	end = 0;
	while (list)
	{
		if (!ft_strcmp(list->name, "##start"))
			begin++;
		if (!ft_strcmp(list->name, "##start"))
			if (!list->next || *list->next->name == '#')
				return (0);
		if (!ft_strcmp(list->name, "##end"))
			end++;
		if (!ft_strcmp(list->name, "##end"))
			if (!list->next || *list->next->name == '#')
				return (0);
		list = list->next;
	}
	if (begin != 1 || end != 1)
		return (0);
	return (check_data_room(begin_list));
}

int		check_data_room(t_room *start)
{
	t_room	*temp;
	char	*name;
	int		x;
	int		y;

	while (start)
	{
		name = start->name;
		x = start->x;
		y = start->y;
		temp = start->next;
		while (temp)
		{
			if (!ft_strcmp(name, temp->name) && *name != '#')
				return (0);
			if (x == temp->x && y == temp->y && *name != '#')
				return (0);
			temp = temp->next;
		}
		start = start->next;
	}
	return (1);
}
