/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solver.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/23 17:04:49 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:29:42 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	solve(int ants, t_room *list_rooms, t_link *list_links)
{
	t_path	**all_path;
	t_path	**ant_path;
	int		i;

	ant_path = NULL;
	all_path = get_all_path(get_start(list_rooms), count_link(list_links));
	leave_unique_only(all_path);
	if (all_path[0])
	{
		ant_path = (t_path **)malloc(sizeof(t_path *) * (ants + 2));
		i = 0;
		while (i <= ants + 1)
			ant_path[i++] = NULL;
		i = 1;
		while (i <= ants)
			ant_path[i++] = choose_path(ant_path, all_path);
		print_input(ants, list_rooms, list_links);
		print_path(ant_path, get_end(list_rooms), ants);
	}
	else
		ft_putstr("Error\n");
	clean_up_path(all_path);
	if (ant_path)
		free(ant_path);
}

void	print_path(t_path **ant_path, t_room *end, int ants)
{
	int		i;
	int		first;

	while (end->ant_in != ants)
	{
		i = 0;
		first = 1;
		while (++i <= ants)
		{
			if (print_move(&ant_path[i], i, first))
				first = 0;
		}
		ft_putchar('\n');
	}
}

int		print_move(t_path **move, int ant_num, int first_flag)
{
	t_path	*temp;

	temp = *move;
	if (!temp || !temp->next)
		return (0);
	if (!temp->next->room->ant_in || temp->next->room->flag == 2)
	{
		if (!first_flag)
			ft_putchar(' ');
		ft_printf("L%i-%s", ant_num, temp->next->room->name);
		*move = temp->next;
		temp->room->ant_in--;
		temp->next->room->ant_in++;
	}
	return (1);
}

t_room	*get_end(t_room *room)
{
	while (room && room->flag != 2)
		room = room->next;
	return (room);
}
