/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_all.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/24 19:38:38 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:31:33 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

t_path	**get_all_path(t_room *start, int count)
{
	t_path	**result;
	t_path	*temp;
	int		i;

	if (!(result = (t_path **)malloc(sizeof(t_path *) * count)))
		return (NULL);
	i = 0;
	while (i <= count)
		result[i++] = NULL;
	temp = NULL;
	find_all(result, &temp, start);
	del_path(temp);
	return (result);
}

void	find_all(t_path **result, t_path **res_list, t_room *current)
{
	int	i;

	if (!add_path(res_list, current))
		return ;
	if (current->flag == 2 && save_path(result, *res_list))
		return ;
	i = 0;
	while (current->links && current->links[i])
	{
		if (check_path(*res_list, current->links[i]))
		{
			find_all(result, res_list, current->links[i]);
			delete_last(*res_list);
		}
		i++;
	}
}

int		save_path(t_path **result, t_path *res_list)
{
	int		i;
	t_path	*temp;

	i = 0;
	while (result[i])
		i++;
	temp = (t_path *)malloc(sizeof(t_path));
	result[i] = temp;
	temp->room = res_list->room;
	temp->next = NULL;
	while (res_list->next)
	{
		temp->next = (t_path *)malloc(sizeof(t_path));
		temp = temp->next;
		res_list = res_list->next;
		temp->room = res_list->room;
		temp->next = NULL;
	}
	return (1);
}

int		check_path(t_path *res_list, t_room *current)
{
	while (res_list)
	{
		if (!ft_strcmp(res_list->room->name, current->name))
			return (0);
		res_list = res_list->next;
	}
	return (1);
}

int		add_path(t_path **res_list, t_room *current)
{
	t_path	*new;
	t_path	*temp;

	temp = *res_list;
	if (!(new = (t_path *)malloc(sizeof(t_path))))
		return (0);
	new->next = NULL;
	new->room = current;
	if (temp)
	{
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
	else
		*res_list = new;
	return (1);
}
