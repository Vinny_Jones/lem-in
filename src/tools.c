/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 15:54:37 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:34:24 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	clean_up_input(char *line, t_room *rooms, t_link *links, t_move *moves)
{
	if (line)
		free(line);
	if (rooms)
	{
		clean_up_input(NULL, rooms->next, NULL, NULL);
		if (rooms->name)
			free(rooms->name);
		if (rooms->links)
			free(rooms->links);
		free(rooms);
	}
	if (links)
	{
		clean_up_input(NULL, NULL, links->next, NULL);
		if (links->link)
			free(links->link);
		free(links);
	}
	if (moves)
	{
		clean_up_input(NULL, NULL, NULL, moves->next);
		if (moves->moves)
			free(moves->moves);
		free(moves);
	}
}

void	print_input(int ants, t_room *room, t_link *link)
{
	ft_printf("%i\n", ants);
	while (room)
	{
		if (*room->name == '#')
			ft_printf("%s\n", room->name);
		else
			ft_printf("%s %i %i\n", room->name, room->x, room->y);
		room = room->next;
	}
	while (link)
	{
		ft_printf("%s\n", link->link);
		link = link->next;
	}
	ft_putchar('\n');
}

t_room	*get_start(t_room *room)
{
	while (room && room->flag != 1)
		room = room->next;
	return (room);
}

int		count_link(t_link *links)
{
	int	count;

	count = 0;
	while (links)
	{
		if (*links->link != '#')
			count++;
		links = links->next;
	}
	return (count * count);
}

int		count_path(t_path *list)
{
	int	count;

	count = 0;
	while (list)
	{
		count++;
		list = list->next;
	}
	return (count);
}
