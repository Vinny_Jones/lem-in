/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_input.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/19 22:43:07 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:31:56 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	parse_input(void)
{
	int		ants;
	char	*line;
	t_room	*list_rooms;
	t_link	*list_links;
	int		err;

	err = 0;
	list_rooms = NULL;
	list_links = NULL;
	line = NULL;
	if ((ants = get_ants_num()) <= 0)
		err = 1;
	while (!err && get_next_line(0, &line) > 0)
		if (check_link(&line, list_rooms))
			break ;
		else if (!check_room(line) || !add_room(&list_rooms, &line, ants))
			err = 1;
	if (!err && !check_logic_room(list_rooms))
		err = 1;
	while (!err && add_link(&line, &list_links) && get_next_line(0, &line) > 0)
		if (*line != '#' && !check_link(&line, list_rooms))
			err = 1;
	if (!err)
		solve(ants, list_rooms, list_links);
	clean_up_input(line, list_rooms, list_links, NULL);
}

int		get_ants_num(void)
{
	char	*line;
	int		i;
	int		result;

	line = NULL;
	result = -1;
	get_next_line(0, &line);
	if (line)
	{
		result = ft_atoi(line);
		i = 0;
		while (line[i])
			if (!ft_isdigit(line[i++]))
				result = -1;
		free(line);
	}
	return (result);
}
