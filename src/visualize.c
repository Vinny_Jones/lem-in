/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/28 16:10:40 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 14:48:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		main(void)
{
	int	ants;

	if ((ants = get_ants_num()) > 0)
		vis_input(ants);
	else
		fmt_print("Error in input data\n", 10, 2, 0);
	return (0);
}

void	vis_input(int ants)
{
	char	*line;
	t_room	*list_rooms;
	int		err;

	err = 0;
	list_rooms = NULL;
	line = NULL;
	while (!err && get_next_line(0, &line) > 0)
		if (check_link(&line, list_rooms))
			break ;
		else if (!check_room(line) || !add_room(&list_rooms, &line, ants))
			err = 1;
	clean_up_input(line, NULL, NULL, NULL);
	if (!err && !check_logic_room(list_rooms))
		err = 1;
	while (!err && get_next_line(0, &line) > 0)
		if (*line != '#' && !check_link(&line, list_rooms))
			break ;
		else
			free(line);
	if (!err && !ft_strcmp(line, ""))
		parse_moves(list_rooms, ants, NULL);
	clean_up_input(line, list_rooms, NULL, NULL);
}

void	visualize(t_room *rooms, t_move *moves)
{
	t_room	*start;
	t_room	*end;
	t_move	*temp;
	char	*line;

	line = NULL;
	temp = moves;
	start = get_start(rooms);
	end = get_end(rooms);
	fmt_print("Room:\n(ANT_NUMBER)room_name-->[lnk_1]...[lnk_N]\n\n", 10, 0, 0);
	printmap(start, end, rooms);
	while (temp && get_next_line(1, &line) > 0 && ft_strcmp(line, "exit"))
	{
		ft_printf("\tCurrent move:\t");
		fmt_print(temp->moves, 10, 6, 0);
		ft_putstr("\n\n");
		make_move(rooms, start, end, temp);
		printmap(start, end, rooms);
		temp = temp->next;
		clean_up_input(line, NULL, NULL, NULL);
	}
	if (!temp)
		fmt_print("\n\t\tDone =)\n\n", 10, 3, 0);
	clean_up_input(NULL, NULL, NULL, moves);
}

void	make_move(t_room *rooms, t_room *start, t_room *end, t_move *move)
{
	char	*temp;
	int		ant_num;

	temp = move->moves;
	while ((temp = ft_strchr(temp, 'L')))
	{
		ant_num = ft_atoi(++temp);
		ant_out(ant_num, rooms, start);
		ant_in(ant_num, ft_strchr(temp, '-') + 1, rooms, end);
	}
}
