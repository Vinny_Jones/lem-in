/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize_parse.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 12:43:34 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 14:40:59 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	parse_moves(t_room *rooms, int ants, t_move *temp)
{
	char	*line;
	t_move	*moves;

	while (get_next_line(0, &line) > 0)
	{
		if (temp)
		{
			temp->next = (t_move *)malloc(sizeof(t_move));
			temp = temp->next;
		}
		else
		{
			temp = (t_move *)malloc(sizeof(t_move));
			moves = temp;
		}
		if (!temp)
			break ;
		temp->moves = ft_strdup(line);
		temp->next = NULL;
		clean_up_input(line, NULL, NULL, NULL);
	}
	if (check_moves(rooms, moves, ants))
		visualize(rooms, moves);
	else if (fmt_print("Error in input data\n", 10, 2, 0))
		clean_up_input(NULL, NULL, NULL, moves);
}

int		check_moves(t_room *rooms, t_move *move, int ants)
{
	char	*temp;
	int		ant_num;

	while (move)
	{
		temp = move->moves;
		while (*temp)
		{
			if (*temp != 'L')
				return (0);
			if ((ant_num = ft_atoi(++temp)) <= 0 || ant_num > ants)
				return (0);
			while (ft_isdigit(*temp))
				temp++;
			if (*temp != '-' || !check_room_vis(rooms, ++temp))
				return (0);
			while (*temp && *temp != ' ')
				temp++;
			if (*temp == ' ')
				temp++;
		}
		move = move->next;
	}
	return (1);
}

int		check_room_vis(t_room *rooms, char *name)
{
	int	len;

	len = 0;
	while (*(name + len) && *(name + len) != ' ')
		len++;
	while (rooms)
	{
		if (!ft_strncmp(rooms->name, name, len))
			return (1);
		rooms = rooms->next;
	}
	return (0);
}
