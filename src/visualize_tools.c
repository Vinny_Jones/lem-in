/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize_tools.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/01 16:30:26 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 14:58:07 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

void	ant_in(int ant_num, char *name, t_room *rooms, t_room *end)
{
	size_t	len;

	len = 0;
	while (*(name + len) && *(name + len) != ' ')
		len++;
	while (rooms)
	{
		if (!rooms->flag && !ft_strncmp(rooms->name, name, len))
			break ;
		rooms = rooms->next;
	}
	if (rooms)
		rooms->ant_in = ant_num;
	else
		end->ant_in++;
}

void	ant_out(int ant_num, t_room *rooms, t_room *start)
{
	while (rooms)
	{
		if (!rooms->flag && ant_num == rooms->ant_in)
			break ;
		rooms = rooms->next;
	}
	if (rooms)
		rooms->ant_in = 0;
	else
		start->ant_in--;
}

void	printmap(t_room *start, t_room *end, t_room *list)
{
	int	i;

	print_room(start, fmt_print("Start room:\n", 1, 7, 0));
	while (list)
	{
		if (!list->flag)
		{
			if (list->ant_in)
				printmap_in(list->ant_in);
			else
				ft_printf("\t(0)");
			ft_putstr("   in room ");
			fmt_print(list->name, 10, 5, 0);
			ft_printf("\n\t    links-->");
			i = -1;
			while (list->links && list->links[++i])
				ft_printf(" [%s]", list->links[i]->name);
			ft_putchar('\n');
		}
		list = list->next;
	}
	print_room(end, fmt_print("End room:\n", 1, 7, 0));
}

void	printmap_in(int ant_num)
{
	ft_printf(FMT_GREEN);
	ft_printf(FMT_BOLD);
	ft_printf("\t(%i)", ant_num);
	ft_printf(FMT_OFF);
}

void	print_room(t_room *room, int code)
{
	int	i;

	ft_printf(FMT_BOLD);
	ft_printf(FMT_BLUE);
	ft_printf("[%s] ", room->name);
	ft_printf(FMT_OFF);
	ft_printf("(Total ");
	ft_printf(FMT_BOLD);
	ft_printf(FMT_YELLOW);
	ft_printf("%i", room->ant_in);
	ft_printf(FMT_OFF);
	ft_printf(" ants inside)\nlinks-->");
	i = -1;
	while (room->links && room->links[++i])
		ft_printf(" [%s]", room->links[i]->name);
	if (code == 12)
		ft_printf("\n");
	else
		ft_printf("\n===========================================\n");
}
