/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_links.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/21 17:53:00 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 12:34:01 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lem_in.h"

int		check_link(char **line, t_room *rooms)
{
	char	*divider;

	if (!*line || **line == '#' || !(divider = ft_strchr(*line, '-')))
		return (0);
	*divider++ = '\0';
	if (check_room_names(*line, divider, rooms))
	{
		*--divider = '-';
		return (1);
	}
	return (0);
}

int		add_link(char **line, t_link **start)
{
	t_link	*temp;
	t_link	*new;

	if (!*line || !(new = (t_link *)malloc(sizeof(t_link))))
		return (0);
	temp = *start;
	if (temp)
	{
		while (temp->next)
			temp = temp->next;
		temp->next = new;
	}
	else
		*start = new;
	if (!(new->link = ft_strdup(*line)))
		return (0);
	new->next = NULL;
	free(*line);
	*line = NULL;
	return (1);
}

int		check_room_names(char *name1, char *name2, t_room *list)
{
	t_room	*room1;
	t_room	*room2;

	room1 = NULL;
	room2 = NULL;
	while (list)
	{
		if (!ft_strcmp(name1, list->name))
			room1 = list;
		if (!ft_strcmp(name2, list->name))
			room2 = list;
		list = list->next;
	}
	if (!room1 || !room2)
		return (0);
	return (link_rooms(room1, room2) && link_rooms(room2, room1));
}

int		link_rooms(t_room *room, t_room *link)
{
	struct s_room	**temp;
	int				i;
	int				j;

	i = 0;
	if (room->links)
		while (room->links[i] && ft_strcmp((room->links[i])->name, link->name))
			i++;
	if (!(i % LINKS) && (!room->links || !room->links[i]))
	{
		temp = room->links;
		room->links = (t_room **)malloc(sizeof(t_room *) * (LINKS + i + 1));
		if (!room->links)
			return (0);
		j = -1;
		while (temp && temp[++j])
			room->links[j] = temp[j];
		j = (j < 0) ? 0 : j;
		while (j <= LINKS + i)
			room->links[j++] = NULL;
		free(temp);
	}
	if (!room->links[i])
		room->links[i] = link;
	return (1);
}
