/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/02/18 21:11:58 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 14:33:07 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H
# include "libft.h"
# define LINKS 5

typedef struct	s_room
{
	int				flag;
	int				x;
	int				y;
	char			*name;
	int				ant_in;
	struct s_room	*next;
	struct s_room	**links;
}				t_room;

typedef struct	s_link
{
	char			*link;
	struct s_link	*next;
}				t_link;

typedef struct	s_path
{
	t_room			*room;
	struct s_path	*next;
}				t_path;

typedef struct	s_move
{
	char			*moves;
	struct s_move	*next;
}				t_move;

void			parse_input(void);
int				get_ants_num(void);
int				check_room(char *line);
int				add_room(t_room **start, char **line, int ants);
t_room			*create_room(char *line);
int				check_logic_room(t_room *list);
int				check_data_room(t_room *start);
int				check_link(char **line, t_room *rooms);
int				check_room_names(char *name1, char *name2, t_room *list);
int				add_link(char **line, t_link **start);
int				link_rooms(t_room *room, t_room *link);
void			clean_up_input(char *line, t_room *r, t_link *l, t_move *m);
void			solve(int ants, t_room *list_rooms, t_link *list_links);

t_path			**get_all_path(t_room *start, int count);
t_room			*get_start(t_room *room);
t_room			*get_end(t_room *room);
int				count_link(t_link *links);
void			print_input(int ants, t_room *room, t_link *link);

void			find_all(t_path **result, t_path **res_list, t_room *current);
int				save_path(t_path **result, t_path *res_list);
int				check_path(t_path *res_list, t_room *current);
int				add_path(t_path **res_list, t_room *current);
void			delete_last(t_path *res_list);

void			sort_path(t_path **all_path);
int				count_path(t_path *list);
void			leave_unique_only(t_path **all_path);
int				check_unique(t_path **path1, t_path **path2);
void			shift_up_elems(t_path **path2);
void			del_path(t_path *del);
void			clean_up_path(t_path **all_path);

t_path			*choose_path(t_path **ant_path, t_path **all_path);
int				moves(t_path *curr, t_path **ant_path);

void			print_path(t_path **ant_path, t_room *end, int ants);
int				print_move(t_path **move, int ant_num, int first_flag);

void			vis_input(int ants);
void			parse_moves(t_room *rooms, int ants, t_move	*temp);
void			visualize(t_room *rooms, t_move *moves);
void			printmap(t_room *start, t_room *end, t_room *list);
void			printmap_in(int ant_num);
void			print_room(t_room *room, int code);
void			make_move(t_room *r, t_room *start, t_room *end, t_move *m);
void			ant_out(int ant_num, t_room *rooms, t_room *start);
void			ant_in(int ant_num, char *name, t_room *rooms, t_room *end);
int				check_moves(t_room *rooms, t_move *moves, int ants);
int				check_room_vis(t_room *rooms, char *name);

#endif
