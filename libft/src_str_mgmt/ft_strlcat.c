/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 16:06:33 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 17:41:46 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t n)
{
	size_t	dst_len;
	size_t	src_len;
	size_t	i;

	src_len = ft_strlen(src);
	dst_len = ft_strlen(dest);
	if (n == 0)
		return (src_len);
	dest += dst_len;
	i = n - MIN(dst_len, n);
	while (--i > 0 && *src)
		*dest++ = *src++;
	*dest = '\0';
	return (src_len + MIN(dst_len, n));
}
