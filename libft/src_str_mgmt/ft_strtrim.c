/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 18:25:45 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	char	*result;
	size_t	len;

	if (!s)
		return (NULL);
	while (*s == ' ' || *s == '\n' || *s == '\t')
		s++;
	len = ft_strlen(s);
	while (*s && (*(s + len - 1) == ' ' || *(s + len - 1) == '\n' \
		|| *(s + len - 1) == '\t'))
		len--;
	result = ft_strnew(len);
	if (!result)
		return (NULL);
	result = ft_strncpy(result, s, len);
	return (result);
}
