/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 11:38:55 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 18:20:35 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t	l_len;

	l_len = ft_strlen(little);
	if (!*little)
		return ((char *)big);
	while (*big)
	{
		if ((ft_strncmp(big, little, l_len)) == 0)
			return ((char *)big);
		else
			big++;
	}
	return (NULL);
}
