/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 16:35:43 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/26 18:28:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*result;
	int		i;

	if (!s)
		return (NULL);
	i = 0;
	result = (char *)malloc(sizeof(char) * ft_strlen(s) + 1);
	if (result)
	{
		while (*s)
		{
			result[i] = f(i, *s);
			i++;
			s++;
		}
		result[i] = '\0';
	}
	return (result);
}
