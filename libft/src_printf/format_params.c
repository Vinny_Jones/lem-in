/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   format_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 14:08:46 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/20 18:16:50 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "ft_printf.h"

int		getwidth(va_list ap_f, va_list ap, char *fmt, int len)
{
	int		res;

	res = parse_width(ap_f, ap, fmt, len);
	while (res > 0 && --len > 0)
		res = (parse_width(ap_f, ap, fmt, len) < 0) ? res * -1 : res;
	return (res);
}

int		parse_width(va_list ap_f, va_list ap, char *fmt, int len)
{
	int		res;
	va_list	temp;

	res = 0;
	va_copy(temp, ap_f);
	while (len > 0)
	{
		if (ft_isdigit(fmt[len]))
		{
			while (ft_isdigit(fmt[len]))
				len--;
			if (fmt[len] != '.' && !fmt_isnumarg(&fmt[len]))
				res = ft_atoi(&fmt[len + 1]);
		}
		if (fmt[len] == '*' && fmt[len - 1] != '.' && fmt_isnumarg(&fmt[len]))
			res = (int)getnumarg(temp, ft_atoi(&fmt[len] + 1), fmt);
		if (fmt[len] == '*' && fmt[len - 1] != '.' && !res)
			res = getnextarg_int(ap, len, fmt);
		if (res || (fmt[len] == '*' && fmt[len - 1] != '.'))
			break ;
		len--;
	}
	va_end(temp);
	return (res);
}

int		getprecision(va_list ap_first, va_list ap, char *fmt, int len)
{
	int		result;
	va_list	temp;

	result = 1;
	va_copy(temp, ap_first);
	while (len > 0)
	{
		if (fmt[len] == '.')
		{
			if (fmt_isnumarg(&fmt[len + 1]))
				result = (int)getnumarg(temp, ft_atoi(&fmt[len] + 2), fmt);
			else if (fmt[len + 1] == '*')
				result = getnextarg_int(ap, len + 1, fmt);
			else if (ft_isdigit(fmt[len + 1]))
				result = ft_atoi(&fmt[len] + 1);
			else
				result = 0;
			break ;
		}
		len--;
	}
	va_end(temp);
	return (result);
}

void	apply_flprec_int(int prec, char *fmt, char **str, int len)
{
	int		i;
	char	*temp;

	len = (**str == '-') ? len - 1 : len;
	prec = (**str == '-') ? prec + 1 : prec;
	if (len < prec)
	{
		temp = *str;
		*str = ft_strnew(prec);
		i = 0;
		while (i < prec - len)
			(*str)[i++] = '0';
		(*temp == '-') ? (*str)[0] = '-' : 0;
		ft_strcat(*str, (*temp == '-') ? temp + 1 : temp);
		free(temp);
	}
	if (CFMT('\'') && len > 3)
		apply_thousand_sep(str, ft_strlen(*str));
	if ((CFMT('#') && (ACFMT('o') || ACFMT('x'))) || CFMT('p'))
		apply_hash_flag(str, fmt, ft_strlen(*str));
	if ((CFMT('+') || CFMT(' ')) && **str != '-')
		apply_sign_flag(fmt, str);
}

void	apply_flwid_int(int width, char *fmt, char **result, int prec)
{
	char	*temp;
	int		i;
	int		len;

	len = ft_strlen(*result);
	if (len < ABS(width))
	{
		temp = *result;
		*result = ft_strnew(ABS(width));
		i = 0;
		while (i < ABS(width))
			(*result)[i++] = ' ';
		i = (CFMT('-') || width < 0) ? 0 : ABS(width) - len;
		ft_strncpy(&(*result)[i], temp, len);
		free(temp);
		if (isnulflag(fmt) && i)
			apply_nul_flag(result, fmt, prec);
	}
}
