/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fmt_mgmt.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/02 13:13:06 by apyvovar          #+#    #+#             */
/*   Updated: 2017/03/02 14:09:15 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		fmt_print(char *text, int bold_line, int color, int bg)
{
	char	*arr_color[9];
	char	*arr_bg_color[9];
	int		bold;
	int		uline;
	int		result;

	bold = bold_line / 10;
	uline = bold_line % 10;
	fmt_init_color(arr_color, arr_bg_color);
	if (bold)
		ft_printf(FMT_BOLD);
	if (uline)
		ft_printf(FMT_ULINE);
	if (color)
		ft_printf(arr_color[color]);
	if (bg)
		ft_printf(arr_bg_color[bg]);
	result = ft_printf(text);
	ft_printf(FMT_OFF);
	return (result);
}

void	fmt_init_color(char **color, char **bg_color)
{
	color[0] = NULL;
	color[1] = FMT_BLACK;
	color[2] = FMT_RED;
	color[3] = FMT_GREEN;
	color[4] = FMT_YELLOW;
	color[5] = FMT_BLUE;
	color[6] = FMT_MAGENTA;
	color[7] = FMT_CYAN;
	color[8] = FMT_WHITE;
	bg_color[0] = NULL;
	bg_color[1] = FMT_BG_BLACK;
	bg_color[2] = FMT_BG_RED;
	bg_color[3] = FMT_BG_GREEN;
	bg_color[4] = FMT_BG_YELLOW;
	bg_color[5] = FMT_BG_BLUE;
	bg_color[6] = FMT_BG_MAGENTA;
	bg_color[7] = FMT_BG_CYAN;
	bg_color[8] = FMT_BG_WHITE;
}
