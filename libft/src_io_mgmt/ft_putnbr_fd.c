/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/26 19:49:08 by apyvovar          #+#    #+#             */
/*   Updated: 2016/11/26 19:50:07 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	if (n < 0)
		ft_putchar_fd('-', fd);
	if (ABS(n) >= 0 && ABS(n) <= 9)
		ft_putchar_fd(ABS(n) + '0', fd);
	else
	{
		ft_putnbr_fd(ABS(n / 10), fd);
		ft_putchar_fd(ABS(n % 10) + '0', fd);
	}
}
