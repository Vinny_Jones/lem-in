LEM-IN
#################################################

The goal of this project is to find the quickest way to get N objects ("ants" in project) across the "map", containing paths and rooms.

Objects need to take the shortest path (and that isn’t necessarily the simplest). They will also need to avoid "traffic jams" as well as "walking over" one another.

At the beginning of the game, all the objects are in the room ##start. The goal is to bring them to the room ##end with as few turns as possible. Each room can only contain one object at a time.

Output structure:

```
#!

[number_of_objects]
[the_rooms name and coordinates]
...
[the_links]
...
Lx-y Lz-w Lr-o ...
```
..., where x, z, r represents the objects’ numbers (going from 1 to [number_of_objects]) and y, w, o represents the rooms’ names.
#################################################
Example:

```
#!

> ./lem-in < subjet2.map
3
2 5 0
##start
0 1 2
##end
1 9 2
3 5 4
0-2
0-3
2-1
3-1
2-3

L1-3 L2-2
L1-1 L2-1 L3-3
L3-1
>
```