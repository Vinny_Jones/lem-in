# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/18 21:15:16 by apyvovar          #+#    #+#              #
#    Updated: 2017/03/02 16:31:55 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

FLAGS = -Wall -Wextra -Werror
CC = gcc $(FLAGS)
NAME = lem-in
VIS_NAME = visu-hex
LIBDIR = libft/
LIB = $(LIBDIR)libft.a
INC = includes
SRC_DIR = src/
SRC_FILES = lem_in.c parse_input.c parse_rooms.c parse_links.c tools.c \
	solver.c search_all.c search_unique.c tools_path.c
SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(SRC:.c=.o)
VIS_SRC_FILES = visualize.c visualize_parse.c visualize_tools.c parse_input.c \
	parse_rooms.c parse_links.c tools.c solver.c search_all.c search_unique.c \
	tools_path.c
VIS_SRC = $(addprefix $(SRC_DIR), $(VIS_SRC_FILES))
VIS_OBJ = $(VIS_SRC:.c=.o)

all: $(NAME)

$(NAME): $(VIS_NAME) $(OBJ)
	make -C $(LIBDIR)
	$(CC) $(OBJ) $(LIB) -o $@

$(VIS_NAME): $(VIS_OBJ)
	make -C $(LIBDIR)
	$(CC) $^ $(LIB) -o $@

.c.o: $(SRC) $(VIS_SRC)
	$(CC) -I $(INC) -I $(LIBDIR)$(INC) -c $^ -o $@

clean:
	make -C $(LIBDIR) clean
	/bin/rm -f $(OBJ) $(VIS_OBJ)

fclean: clean
	/bin/rm -f $(LIB)
	/bin/rm -f $(NAME)
	/bin/rm -f $(VIS_NAME)

re: fclean all
